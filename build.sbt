name := "LinAlg-Examples"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
	"de.h2b.scala.lib.math" %% "linalg" % "2.0.0",
	"de.h2b.scala.lib" %% "utilib" % "0.2.0"
)
