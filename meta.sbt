organization := "de.h2b.scala.lib.math"

organizationName := "private"

organizationHomepage := Some(url("http://h2b.de"))

homepage := Some(url("http://h2b.de"))

startYear := Some(2015)

description := """
These are examples demonstrating the use of LinAlg, the Scala library of 
algebraic vectors and matrices.
"""

licenses := Seq("European Union Public Licence, v. 1.1" -> url("https://joinup.ec.europa.eu/community/eupl/og_page/eupl"))

pomExtra := Seq(
	<scm>
		<url>scm:git:https://gitlab.com/h2b/LinAlgExamples.git</url>
	</scm>,
	<developers>
		<developer>
			<id>h2b</id>
			<name>Hans-Hermann Bode</name>
			<email>projekte@h2b.de</email>
			<url>http://h2b.de</url>
			<roles>
				<role>Owner</role>
				<role>Architect</role>
				<role>Developer</role>
			</roles>
			<timezone>Europe/Berlin</timezone>
		</developer>
	</developers>
)
