/*
  LinAlgExamples -- Examples Using the LinAlg Scala Library

  Copyright 2015 Hans-Hermann Bode

  Licensed under the EUPL V.1.1.
*/

package de.h2b.scala.linalg.example

import de.h2b.scala.lib.math.linalg.Matrix
import de.h2b.scala.lib.math.linalg.Vector

/**
 * Demonstrates the use of LinAlg, the scala library of algebraic vectors and
 * matrices by solving linear equation systems by means of the Jacobi method.
 *
 * @see [[https://en.wikipedia.org/wiki/Jacobi_method]]
 *
 * @author h2b
 */
object Jacobi {

  /**
   * Solves the linear equation system `A x = b` by iterations using the Jacobi
   * method.
   *
   * @param A
   * @param b
   * @param eps the accuracy of the solution measured by the Euklidian norm
   * @return the solution vector
   */
  def solution (A: Matrix[Double], b: Vector[Double], eps: Double): Vector[Double] = {
    val D = diag(A) //diagonal part
    check(D) //must not have zero elements
    val R = A-D //rest
    val Dinv = D.map(_.map(1/_))
  	var x = Vector[Double]()
  	while ((A*x-b).norm>eps) x = Dinv*(b-R*x)
  	x
  }

  private def diag (A: Matrix[Double]) = {
    val seq = for (i ← A.index.dim1.low to A.index.dim1.high) yield Vector.at(i)(A(i,i))
    Matrix(seq: _*)
  }

  private def check (D: Matrix[Double]) =
    D.foreach(_.foreach((x: Double) ⇒ require(x.compareTo(0.0)!=0, "diagonal part has zeroes")))

}