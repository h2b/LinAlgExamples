/*
  LinAlgExamples -- Examples Using the LinAlg Scala Library

  Copyright 2015 Hans-Hermann Bode

  Licensed under the EUPL V.1.1.
*/

package de.h2b.scala.linalg.example

import scala.math.abs

import de.h2b.scala.lib.math.linalg.{ Matrix, Vector }
import de.h2b.scala.lib.math.smallDouble

/**
 * @see [[https://en.wikipedia.org/wiki/Jacobi_method]]
 * @author h2b
 */
object JacobiRun extends App {

  private val epsilon = smallDouble*10

  private var count = 0

  private def run (A: Matrix[Double], b: Vector[Double]) = {
    count += 1
    val result = Jacobi.solution(A, b, epsilon)
    val deviation = (A*result-b).norm
    println(s"* Run $count")
    println(s"result=$result")
    println(s"deviation from solution=$deviation")
    if (abs(deviation)<=epsilon) println("passed") else println("NOT PASSED")
    println
  }

  {
    val A = Matrix(Vector(2.0, 1.0 ), Vector(5.0, 7.0))
    val b = Vector(11.0, 13.0)
    run(A, b)
  }

  {
    val A = Matrix(Vector(10.0, -1.0, 2.0), Vector(-1.0, 11.0, -1.0, 3.0),
        Vector(2.0, -1.0, 10.0, -1.0), Vector(0.0, 3.0, -1.0, 8.0))
    val b = Vector(6.0, 25.0, -11.0, 15.0)
    run(A, b)
  }

}