# LinAlgExamples -- Examples Using the LinAlg Scala Library

These are examples demonstrating the use of LinAlg, the scala library of algebraic vectors and matrices. Currently, there is only one example.

## Solving a System of Linear Equations

In this example, we use the [Jacobi method](https://en.wikipedia.org/wiki/Jacobi_method) to solve the system <span class="math"><em>A</em><em>x</em> = <em>b</em></span>
of linear equations by iterations.

By decompostion of <span class="math"><em>A</em></span> into its diagonal
<span class="math"><em>D</em></span> and the rest <span class="math"><em>R</em></span>,
i.e., <span class="math"><em>A</em> = <em>D</em> + <em>R</em></span>, we get the 
iterative formula:

<span class="math"><em>x</em><sup><em>k</em> + 1</sup> = <em>D</em><sup> − 1</sup>(<em>b</em> − <em>R</em><em>x</em><sup><em>k</em></sup>)</span>

So, how do we implement this with the LinAlg Scala Library? Assuming, we had 
already calculated `Dinv` (<span class="math"><em>D</em><sup> − 1</sup></span>)
and `R`, it is very simple; starting from an empty vector (which is essentially
the zero vector in LinAlg), just iterate until the Euklidian norm (an instance 
method provided by `Vector`) of the solution inserted into the system is below 
a given accuracy `eps`:

    var x = Vector[Double]()
    while ((A*x-b).norm>eps) x = Dinv*(b-R*x)

As we see, we nearly have a one-to-one mapping of code and mathematics.
     
To get the diagonal, we loop throw the rows of `A`, pick from each the 
appropriate element and put it into a one-element vector at the right lower 
index bound:

    val diag = for (i ← A.index.dim1.low to A.index.dim1.high) yield Vector.at(i)(A(i,i))
    
From the resulting sequence of vectors, we get the diagonal matrix by

    val D = Matrix(diag: _*)
    
Since matrices as well as vectors have index ranges in LinAlg, `D` is exactly
the diagonal matrix required with only its diagonal elements stored.

Now, `R` can be can be computed by

    val R = A-D

Finally, to get `Dinv`, we remember that the inverse of a diagonal matrix has
just its elements inverted. Further, we  make use of the fact that both vectors 
and matrices in LinAlg are iterables so that they inherit the `map` function.
Thus, we can write

    val Dinv = D.map(_.map(1/_))
    
The first `map` applies to the row vectors of `D`, the second one to the
elements of a row vector.

## Licence

LinAlgExamples -- Examples Using the LinAlg Scala Library

Copyright 2015 Hans-Hermann Bode

Licensed under the EUPL V.1.1.
